#!/usr/bin/python
# obtain the URL information by submitting plain text queries to the 
# Bing API. 
# Input: a text file, each line is a query string
# Output: a text file in xml format, containing the following fields
# * queryid: yyyymmddhhMMss_n (n is set when multiple queries are generated in the same second)
# * querytext 
# * url: urls returned by the search
# By default, the program also writes all the results in each query into a 
# file in JSON format. One output file for each query is generated. This 
# may take more space but it preservers all the query results information so
# we do not need to query these terms again (for a short period) if we need to.
#
from bing_search_api import BingSearchAPI
import json
from xml.dom.minidom import getDOMImplementation
from datetime import datetime 
import os
import printinfo
import urllib
import urllib2
import time
import counter

class queryResult(object):
    
    def __init__(self):
	self.qid = None
	self.urls = None
	self.queryterm = None

    def setQID(self,qid):
	self.qid = qid
 
    def setUrls(self,urls):
	self.urls = urls

    def setQueryTerm(self,qt):
	print 'qt = ',qt
	self.queryterm = qt
	print 'queryterm = ',self.queryterm


def startup(removestopwords=True,verbal=False):
    # initial setups
    # query file contains the query strings
    queryfile = '/home/jxw394/project/bingquerydblp/data/dblp-title-random-3k-facet-title-set2.txt'
    # query log contains the main query results (qid,querytext,url)
    querylog = 'querylog.json'
    # stopwords file contains most commonly used stopwords in search engines
    stopwordsfile = 'stopwords.txt'
    # main output directory, relative to the current directory
    mainoutdir = '../output/bing_search'
    # delimeter of input file. if this deli is found, the file is identified
    # as a two column file and the first column is treated as an ID
    # if this deli meter is not found, the file is identified as a single
    # column file and an ID is automatically assigned to each query string. 
    deli = '::'
    # filetype: pdf/ppt/doc etc.
    filetype = 'pdf'

    # create directory if they do not exist
    if not os.path.exists(mainoutdir):
	os.makedirs(mainoutdir)
 
    # relative to the main output directory, query results (JSON files) 
    # are saved to this sub-directory named with yyyy-mm-dd-HH-MM-SS
    jsonoutdir = mkdirdatetime(mainoutdir)

    # load stop words
    stopwords = open(stopwordsfile).read().split()

    # create the counter object
    counters = counter.Counter()
    counters.newCounter("failed")
    counters.newCounter("success")
    counters.newCounter("no-empty-result")
    counters.newCounter("URLs")

    # if json output is required, create the directory if it does not exist
    #if not os.path.exists(jsonoutdir):
    #    os.makedirs(jsonoutdir)

    # create bing search object
    #bing = BingSearchAPI(my_key)
    #params = {'$format':'json','$skip':0,'$stop':searchstop}
    
    # write results into xml file
    impl = getDOMImplementation()

    # create query result array
    qrs = []

    # create message printer
    infoprinter = printinfo.printInfo()

    # load query file
    try:
	with open(queryfile,'r') as qf:
	   linei = 0
	   for line in qf:
		# trim off the cartridge return
		line = line.strip('\n')
		# if comma is found in the line string, identify the file in two columns
	  	# take the first column as an ID and the second as the query string.
		# if comma is NOT found, identify the file in one column. Assign an ID
		# to the title using an auto incremental integer (equals to line number)
		if line.find(deli) == -1:
		    doi = str(linei)
		    title = line
		else:
		    # split the DOI (or query id) with the query string
		    doi = line[0:line.find(deli)]
		    title = line[line.find(deli)+1:]
    		# create the query result object
		qr = {u'doi':doi,u'title':u'',u'urls':[]}
		qr['title'] = title
		qrs.append(qr)
		linei += 1
    except IOError:
	infoprinter.printStatus('load queryfile','fail')
	infoprinter.printPara('queryfile',queryfile)
	raise
	
    # perform query
    counters.setCounter('all',len(qrs))
    for qr in qrs:
	# remove stopwords if desired
 	if removestopwords:
    	    # split the input 
    	    textwords = qr['title'].split()
    	    # remove stopwords
    	    filteredtextwords = [t for t in textwords if t.lower() not in stopwords]
    	    # re-join the text words 
    	    queryterm = ' '.join(filteredtextwords)
	else:
	    queryterm = qr['title']

	# perform bing search
	r = bing_search(queryterm,'Web','pdf')
	if r is None:
	    infoprinter.printStatus("query",'fail')
	    print "json file not created"
	    counters.addCounter("failed")
	    continue
	counters.addCounter("success")

	# write results into the JSON file
	jsonoutfile = os.path.join(jsonoutdir,qr['doi']+'.json')
	with open(jsonoutfile,'w') as jf:
	    json.dump(r,jf)
	    jf.flush()

	# extract Urls from query results
	urls = []
	print queryterm,': ',len(r)
	if len(r) >=1:
	    counters.addCounter("no-empty-result")
	    counters.addCounter("URLs",len(r))
	for i in range(0,49):
	    try: 
	        url = r[i]['Url']
	        urls.append(url)
	    except IndexError:
		break

	qr['urls'] = urls

    # write the json data into a file
    with open(os.path.join(jsonoutdir,querylog),'w') as lf:
        json.dump(qrs,lf)
    lf.close()
    
    # end
    counters.printCounter()
    print 'query log is written to: '+os.path.join(jsonoutdir,querylog)
    print 'query results are written to: '+jsonoutdir
    print 'program end' 


# get a datetime string which is in form of yyyy-mm-dd-HH-MM-SS 
# if this function is called multiple times within the 
# same second, wait for one second 
# (not the best method but easier to program and acceptable)
def getcurrentdatetimestr():
    # get the current time
    cdt = datetime.now()
    cdtstr = cdt.strftime('%Y-%m-%d-%H-%M-%S')
    return cdtstr

# create a directory under the "dir" named with 
# the current datetime string
def mkdirdatetime(dir):
    dirmade = False 
    while not dirmade:
      	dirpath = os.path.join(dir,getcurrentdatetimestr())
    	if os.path.exists(dirpath):
	    time.sleep(1)
	else:
	    os.mkdir(dirpath)
	    dirmade = True
        #dirpath = os.path.join(dir,getcurrentdatetimestr)
	
    return dirpath
	

# submit a query to bing search api       
# search type could be 'Web', 'Image', 'News', 'Video' etc. 
#
def bing_search(query, search_type, filetype):
    #search_type: Web, Image, News, Video
    # fanchyna@hotmail.com
    #key = 'EaaMgKJ1FGpUZfmqE66IbBng1w69X0wVGqSMefMN9bQ'
    # halfmoon81@live.com
    #key = 'ReV7WMWd/zdQKYhA6gB/dlyl5lg1uJ5aOBoenA4EP+8'
    # i.buy@hotmail.com
    key = 'mWrIPpidA0N6WzNDLfGAE8XfFEeidUcgBIX9YflbAVM'

    query = urllib.quote(query)

    # create credential for authentication
    user_agent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; FDM; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 1.1.4322)'
    credentials = (':%s' % key).encode('base64')[:-1]
    auth = 'Basic %s' % credentials
    url = 'https://api.datamarket.azure.com/Data.ashx/Bing/Search/'+search_type+'?Query=%27'+query+'+filetype:'+filetype+'%27&$top=50&$format=json'
    result_list = []
    try:
        request = urllib2.Request(url)
    	request.add_header('Authorization', auth)
    	request.add_header('User-Agent', user_agent)
    	request_opener = urllib2.build_opener()
    	response = request_opener.open(request) 
    	response_data = response.read()
    	json_result = json.loads(response_data)
    	result_list = json_result['d']['results']
	return result_list
    except urllib2.HTTPError:
	print "fail to query URL"
	print url
	return None

if __name__ == '__main__':

    startup(removestopwords=True,verbal=True)
